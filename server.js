const express = require('express');
const cors = require('cors');
const logger = require("morgan");
const app = express();

const port = process.env.port||3000;

const users = [{login: "TasteOfOil", password:"1234567"},
            {login: "login", password:"password"}];

app.listen(port, ()=>{
    console.log(`Listening on port: ${port}`);
})

app.use(cors({
    origin:'*'
}));

app.use(logger("dev"));

app.get('/',(req, res)=>{
    res.send("Hello World");
})

app.get('/user/:login/:password', (req,res)=>{
    const result =  users.find((elem,index,array)=>{
        return elem.login === req.params.login && elem.password === req.params.password; 
    })
    if(!result) throw new Error('Wrong login or password');
    res.send("You are logged in");
})

app.use('/user/:login/:password', (err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send("Login or password entered incorrectly");
});

